(* enumerated type declaration *)
datatype seasoning =
	 Salt
       | Pepper;

(* union type declaration *)
datatype num =
	 Zero
	 | One_more_than of num;		      

One_more_than(One_more_than(Zero)); (* num *)

(* polymorphic type declaration *)
datatype 'a open_faced_sandwich =
	 Bread of 'a
	 | Slice of 'a open_faced_sandwich;

Bread(0);
Bread(true);
Slice(Slice(Bread(Bread(One_more_than(Zero)))));
