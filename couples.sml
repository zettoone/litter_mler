Control.Print.printDepth := 20;

datatype 'a pizza =
	 Bottom
	 | Topping of ('a * ('a pizza));

datatype fish =
	 Anchovy
       | Lox
       | Tuna;

Topping(Anchovy, Topping(Lox, Topping(Tuna, Topping(Anchovy, Bottom))));

fun rem_anchovy(Bottom)
    = Bottom
  | rem_anchovy(Topping(Anchovy, p))
    =  rem_anchovy(p)
  | rem_anchovy(Topping(t, p))
    = Topping(t, rem_anchovy(p));

rem_anchovy(Topping(Lox, Topping(Anchovy, Topping(Tuna, Topping(Anchovy, Bottom)))));
rem_anchovy(Topping(Lox, Topping(Tuna, Bottom)));

(* more general version *)
fun eq_fish(Anchovy, Anchovy)
    = true
  | eq_fish(Lox, Lox)
    = true
  | eq_fish(Tuna, Tuna)
    = true
  | eq_fish(a, b)
    = false; 

fun rem_fish(x, Bottom)
    = Bottom
  | rem_fish(x, Topping(t, p))
    = if eq_fish(x, t)
      then rem_fish(x, p)
      else Topping(t, rem_fish(x, p));

rem_fish(Anchovy, Topping(Anchovy, Topping(Anchovy, Bottom)));
rem_fish(Tuna, Topping(Anchovy, Topping(Tuna, Topping(Anchovy, Bottom))));

(* substitute *)
fun subst_fish(n, a, Bottom)
    = Bottom
  | subst_fish(n, a, Topping(t, p))
    = if eq_fish(a, t)
      then Topping(n, subst_fish(n, a, p))
      else Topping(t, subst_fish(n, a, p));

subst_fish(Lox, Anchovy,
	   Topping(Anchovy, Topping(Tuna, Topping(Anchovy, Bottom))));

(* play with num *)
datatype num =
	 Zero
       | One_more_than of num;

fun eq_num(Zero, Zero)
    = true
  | eq_num(One_more_than(m), One_more_than(n))
    = eq_num(m, n)
  | eq_num(m, n)
    = false;

