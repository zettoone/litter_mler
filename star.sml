Control.Print.printDepth := 20;

datatype meza =
	 Shrimp
       | Calamari
       | Escargots
       | Hummus;

datatype main =
	 Steak
       | Ravioli
       | Chicken
       | Eggplant;

datatype salad =
	 Green
       | Cucumber
       | Greek;

datatype dessert =
	 Sundae
       | Mousse
       | Torte;

(* more general but less precise *)
fun add_a_steak(x)
    = (x, Steak);

fun eq_main(Steak, Steak)
    = true
  | eq_main(Ravioli, Ravioli)
    = true
  | eq_main(Chicken, Chicken)
    = true
  | eq_main(Eggplant, Eggplant)
    = true
  | eq_main(a_main, another_main)
    = false;

fun has_steak(a:meza, Steak, d:dessert):bool
    = true
  | has_steak(a:meza, m:main, d:dessert):bool
    = false;

has_steak((Shrimp, Steak, Mousse));
has_steak((Calamari, Chicken, Torte));
  
