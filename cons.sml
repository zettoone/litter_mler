Control.Print.printDepth := 20;

datatype pizza =
	 Crust
	 | Cheese of pizza
	 | Onion of pizza
	 | Anchovy of pizza
	 | Sausage of pizza;

fun remove_anchovy(Crust)
    = Crust
  | remove_anchovy(Cheese(x))
    = Cheese(remove_anchovy(x))
  | remove_anchovy(Onion(x))
    = Onion(remove_anchovy(x))
  | remove_anchovy(Anchovy(x))
    = remove_anchovy(x)
  | remove_anchovy(Sausage(x))
    = Sausage(remove_anchovy(x));

val no_anchovy = Onion(Sausage(Cheese(Onion(Cheese(Sausage(Crust))))));
remove_anchovy(no_anchovy);
val sample_pizza = Anchovy(Onion(Anchovy(Anchovy(Cheese(Crust)))));
remove_anchovy(sample_pizza);

fun top_anchovy_with_cheese(Crust)
    = Crust
  | top_anchovy_with_cheese(Cheese(x))
    = Cheese(top_anchovy_with_cheese(x))
  | top_anchovy_with_cheese(Onion(x))
    = Onion(top_anchovy_with_cheese(x))
  | top_anchovy_with_cheese(Anchovy(x))
    = Cheese(Anchovy(top_anchovy_with_cheese(x)))
  | top_anchovy_with_cheese(Sausage(x))
    = Sausage(top_anchovy_with_cheese(x));

val anchovy_pizza = Onion(Anchovy(Cheese(Anchovy(Crust))));
top_anchovy_with_cheese(anchovy_pizza);

fun subst_anchovy_by_cheese(x)
    = remove_anchovy(top_anchovy_with_cheese(x));

subst_anchovy_by_cheese(anchovy_pizza);
